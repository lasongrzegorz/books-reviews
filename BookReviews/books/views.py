from django.db.models import Avg, Count
from django.shortcuts import reverse
from django.contrib.auth.mixins import AccessMixin
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import FormView
from django.views.generic.detail import SingleObjectMixin

from reviews.forms import RatingForm
from books.models import Author, Book, Genre


class IndexView(TemplateView):
	template_name = 'index.html'


class AuthorsView(ListView):
	template_name = 'books/authors.html'
	queryset = Author.objects.all()
	context_object_name = 'authors'


class AuthorDetailView(DetailView):
	model = Author
	template_name = 'books/author_details.html'
	# context_object_name = 'author'

	def get_context_data(self, **kwargs):
		context = {
			'title': f'Author {self.object}',
		}
		context.update(kwargs)
		return super().get_context_data(**context)


class BooksView(ListView):
	template_name = 'books/books.html'
	queryset = Book.objects.all()
	context_object_name = 'books'


class BookDetailView(AccessMixin, SingleObjectMixin, FormView):
	model = Book
	template_name = 'books/book_details.html'
	form_class = RatingForm
	permission_denied_message = 'Custom permission denied message'

	def get_success_url(self):
		return reverse('books:book_detail', kwargs={'pk': self.object.pk})

	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		context = self.get_context_data(object=self.object)
		return self.render_to_response(context)

	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return self.handle_no_permission()
		self.object = self.get_object()
		return super().post(request, *args, **kwargs)

	def form_valid(self, form):
		form.instance.user = self.request.user
		form.instance.book = self.object
		self.rating = form.save()
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		context = {
			'title': f'Book {self.object}',
			'avg_ratings': self.object.rating_set.aggregate(
				average=Avg('rating'), count=Count('rating')
			),
		}
		context.update(kwargs)
		return super().get_context_data(**context)


class GenresView(ListView):
	template_name = 'books/genres.html'
	queryset = Genre.objects.all()
	context_object_name = 'genres'


class GenreDetailView(DetailView):
	model = Genre
	template_name = 'books/genre_details.html'
	context_object_name = 'genres'
