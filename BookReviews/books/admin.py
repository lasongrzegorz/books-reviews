from django.contrib import admin
from django.db import models
from .models import Author, Book, Genre
from reviews.models import Rating

from tinymce import AdminTinyMCE


class BookInline(admin.TabularInline):
	model = Book
	# extra = 0
	formfield_overrides = {
		models.TextField: {
			'widget': AdminTinyMCE(
				mce_attrs={
					'height': 80,
				}
			),
		},
	}

	readonly_fields = [
		'added',
		'updated',
	]
	ordering = ['title']

	def get_extra(self, request, obj=None, **kwargs):
		return 0 if obj else 1


class AuthorAdmin(admin.ModelAdmin):
	date_hierarchy = 'created'
	list_display = [
		'first_name',
		'last_name',
		'birth_date',
		'website',
		'email',
		'rating',
	]
	list_filter = [
		'rating',
	]
	search_fields = [
		'first_name',
		'last_name',
		'email',
	]
	ordering = ['last_name']

	fieldsets = [
		(None, {
			'fields': [('first_name', 'last_name', 'rating', 'birth_date')]
		}),
		('Contact details', {
			'fields': [('website', 'email')],
			'classes': ['collapse']
		}),
	]

	inlines = [BookInline]


class RatingInline(admin.TabularInline):
	model = Rating

	readonly_fields = [
		'user',
		'rating',
	]

	def get_extra(self, request, obj=None, **kwargs):
		return 0 if obj else 1


class BookAdmin(admin.ModelAdmin):

	formfield_overrides = {
		models.TextField: {
			'widget': AdminTinyMCE(
				mce_attrs={
					'height': 80,
				}
			),
		},
	}

	date_hierarchy = 'added'
	list_display = [
		'title',
		'author',
		'book_cover',
		'pages',
		'added',
		'updated',
	]
	list_filter = [
		'rating',
	]
	search_fields = [
		'title',
		'author__last_name',
	]
	ordering = ['title']

	fieldsets = [
		(None, {
			'fields': [('title', 'author', 'added', 'updated')]
		}),
		('Book details', {
			'fields': [('pages', 'genres', 'description'), 'book_cover'],
		}),
	]

	readonly_fields = [
		'added',
		'updated',
		'author',
	]
	inlines = [RatingInline]


admin.site.register(Author, AuthorAdmin)
admin.site.register(Genre)
admin.site.register(Book, BookAdmin)
