from django.urls import path
from . import views

app_name = 'books'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('authors/', views.AuthorsView.as_view(), name='authors_list'),
    path('authors/<int:pk>', views.AuthorDetailView.as_view(), name='author_details'),
    path('books/', views.BooksView.as_view(), name='books_list'),
    path('books/<int:pk>', views.BookDetailView.as_view(), name='book_detail'),
    path('genres/', views.GenresView.as_view(), name='genres_list'),
    path('genres/<int:pk>', views.GenreDetailView.as_view(), name='genre_details'),
]
