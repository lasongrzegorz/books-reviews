from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone


class Author(models.Model):
	first_name = models.CharField(
		max_length=64,
		help_text='',
		verbose_name='First name',
	)
	last_name = models.CharField(
		max_length=64,
		help_text='',
		verbose_name='Last name',
	)
	about = models.TextField(
		blank=True,
		null=True,
		help_text='',
		verbose_name='About author',
	)
	birth_date = models.DateField(
		blank=True,
		null=True,
		help_text='',
		verbose_name='Birth date',
	)
	website = models.URLField(
		blank=True,
		null=False,
		default='',
		help_text='',
		verbose_name='Website',
	)
	email = models.EmailField(
		blank=True,
		null=True,
		help_text='',
		verbose_name='Email',
	)
	rating = models.PositiveSmallIntegerField(
		blank=True,
		null=True,
		help_text='',
		verbose_name='Rating',
		validators=[
			MinValueValidator(0),
			MaxValueValidator(10),
		]
	)
	created = models.DateTimeField(
		default=timezone.now,
		verbose_name='Created',
	)
	updated = models.DateTimeField(
		default=timezone.now,
		verbose_name='Last update',
	)

	def save(self, *args, **kwargs):
		self.updated = timezone.now()
		super(Author, self).save(*args, **kwargs)

	def __str__(self):
		return f'{self.first_name} {self.last_name}'


class Book(models.Model):
	title = models.CharField(
		max_length=200,
		help_text='',
		verbose_name='Title',
	)
	author = models.ForeignKey(
		Author, on_delete=models.CASCADE,
		help_text='',
		verbose_name='Author',
	)
	genres = models.ManyToManyField(
		'Genre',
		default='General',
		help_text='',
		verbose_name='Genre',
	)
	pages = models.PositiveSmallIntegerField(
		blank=True,
		null=True,
		verbose_name='Pages',
	)
	description = models.TextField(
		blank=True,
		null=True,
		help_text='',
		verbose_name='Description',
	)
	added = models.DateTimeField(
		default=timezone.now,
		verbose_name='Added on',
	)
	updated = models.DateTimeField(
		default=timezone.now,
		verbose_name='Last update',
	)
	book_cover = models.ImageField(
		upload_to='static',
		default='',
		blank=True,
		null=True,
		verbose_name='Cover image',
		help_text='',
	)

	def save(self, *args, **kwargs):
		self.updated = timezone.now()
		super(Book, self).save(*args, **kwargs)

	def __str__(self):
		return f'{self.title}'


class Genre(models.Model):
	name = models.CharField(
		max_length=64,
		default='General',
		help_text='',
		verbose_name='Name',
	)
	description = models.TextField(
		blank=True,
		null=True,
		help_text='',
		verbose_name='Description',
	)
	added = models.DateTimeField(
		default=timezone.now,
		verbose_name='Added on',
	)

	def __str__(self):
		return self.name
