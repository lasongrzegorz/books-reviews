from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect, reverse
from django.views.generic import UpdateView, View, FormView
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView
from django.urls import reverse_lazy

from books.models import Book
from .models import Review
from .forms import ReviewForm


class ListReviews(LoginRequiredMixin, ListView):
	template_name = 'reviews/reviews.html'
	context_object_name = 'reviews'

	def get_queryset(self):
		return Review.objects.filter(user=self.request.user)


class AddReview(LoginRequiredMixin, CreateView):
	model = Review
	form_class = ReviewForm
	template_name = 'reviews/review_add.html'
	success_url = reverse_lazy('reviews:reviews_list')

	def get_context_data(self, **kwargs):
		form = ReviewForm(instance=self.get_book())
		context = {
			'title': f'Edit review of {self.book}',
			'form': form,
		}
		context.update(kwargs)
		return super().get_context_data(**context)

	def get_book(self):
		book_id = self.kwargs.get('book_id')
		return get_object_or_404(Book, pk=book_id)

	def get(self, request, *args, **kwargs):
		self.book = self.get_book()
		return super().get(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		self.book = self.get_book()

		return super().post(request, *args, **kwargs)

	def form_valid(self, form):
		form.instance.user = self.request.user
		form.instance.book = self.book
		form.instance.status = 'draft'
		return super().form_valid(form)


class PublishReview(LoginRequiredMixin, View):

	def post(self, request, *args, **kwargs):
		review_id = self.kwargs.get('review_id')
		review = get_object_or_404(
			Review,
			pk=review_id,
			user=request.user,
			status='draft',
		)
		review.status = 'published'
		review.save()
		return redirect('reviews:reviews_list')


class EditReview(LoginRequiredMixin, UpdateView):
	model = Review
	template_name = 'reviews/review_edit.html'
	pk_url_kwarg = 'review_id'
	fields = ['subject', 'content', 'rating']
	success_url = reverse_lazy('reviews:reviews_list')

	def get_queryset(self):
		review_id = self.kwargs.get('review_id')
		return Review.objects.filter(id=review_id)

	def form_valid(self, form):
		form.instance.user = self.request.user
		form.instance.book = self.object.book
		form.instance.status = 'draft'
		return super().form_valid(form)

	def get_object(self, queryset=None):
		return Review.objects.get(id=self.kwargs['review_id'])

	def get_context_data(self, **kwargs):
		form = ReviewForm(instance=self.get_object())
		context = {
			'title': f'Edit review',
			'form': form,
		}
		return super().get_context_data(**context)


class DeleteReview(LoginRequiredMixin, DeleteView):
	model = Review
	pk_url_kwarg = 'review_id'
	success_url = reverse_lazy('reviews:reviews_list')
