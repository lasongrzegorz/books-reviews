from django.contrib import admin
from django.db import models
from .models import Review, Rating

from tinymce import AdminTinyMCE


class ReviewAdmin(admin.ModelAdmin):

	formfield_overrides = {
		models.TextField: {
			'widget': AdminTinyMCE(
				mce_attrs={
					'height': 180,
				}
			),
		},
	}

	date_hierarchy = 'published'
	list_display = [
		'user',
		'subject',
		'published',
		'content',
		'status',
		'rating',
		'added',
		'updated',
	]
	list_filter = [
		'rating',
		'user',
	]
	search_fields = [
		'user',
		'subject',
	]
	ordering = ['user']
	readonly_fields = [
		'added',
		'updated',
	]
	list_editable = [
		'status'
	]

	fieldsets = [
		(None, {
			'fields': [('user',)]
		}),
		('Status', {
			'fields': [('status', 'published', 'added', 'updated')],
		}),
		('Review details', {
			'fields': [('book', 'rating'), 'subject', 'content'],
		}),
	]


admin.site.register(Review, ReviewAdmin)
admin.site.register(Rating)
