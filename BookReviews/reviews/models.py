from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone

from books.models import Book


class ReviewManager(models.Manager):
	def published(self):
		return self.filter(status='published').order_by('-added')


class Review(models.Model):

	STATUS_CHOICES = [
		('draft', 'Draft'),
		('sent', 'Sent'),
		('rejected', 'Rejected'),
		('published', 'Published'),
	]

	user = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		on_delete=models.PROTECT,
		verbose_name='User',
		help_text='',
	)
	book = models.ForeignKey(
		Book, on_delete=models.CASCADE,
		verbose_name='Book',
		help_text='',
	)
	subject = models.CharField(
		max_length=200,
		verbose_name='Subject',
		help_text='',
	)
	content = models.TextField(
		verbose_name='Content',
		help_text='',
	)
	published = models.DateTimeField(
		default=timezone.now,
		verbose_name='Published',
	)
	status = models.CharField(
		choices=STATUS_CHOICES,
		max_length=64,
		verbose_name='Status',
		help_text='',
	)
	rating = models.PositiveSmallIntegerField(
		verbose_name='Rating',
		help_text='Values from 0 to 10',
		validators=[
			MinValueValidator(0),
			MaxValueValidator(10),
		]
	)
	added = models.DateTimeField(
		default=timezone.now,
		verbose_name='Added on',
	)
	updated = models.DateTimeField(
		default=timezone.now,
		verbose_name='Last update',
	)

	def save(self, *args, **kwargs):
		self.updated = timezone.now()
		if self.status == 'published' and not self.published:
			self.published = timezone.now()
		return super().save(*args, **kwargs)

	def __str__(self):
		return f'{self.book} | {self.subject}'

	objects = ReviewManager()


class Rating(models.Model):

	user = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		verbose_name='User',
		help_text='',
	)
	rating = models.PositiveSmallIntegerField(
		verbose_name='Rating',
		help_text='Values from 0 to 10',
		validators=[
			MinValueValidator(0),
			MaxValueValidator(10),
		],
	)
	book = models.ForeignKey(
		Book, on_delete=models.CASCADE,
		verbose_name='Book',
		help_text='',
	)

	def __str__(self):
		return f'Rating: {self.rating} | Book: {self.book}'
