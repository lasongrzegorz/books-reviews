from django import forms

from .models import Rating, Review


class RatingForm(forms.ModelForm):
	# rating = forms.IntegerField(max_value=10, min_value=0)

	class Meta:
		model = Rating
		fields = [
			'rating',
		]

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['rating'].widget = forms.NumberInput(
			attrs={'min': 0, 'max': 10}
		)


class ReviewForm(forms.ModelForm):

	class Meta:
		model = Review
		fields = [
			'subject',
			'content',
			'rating',
		]
