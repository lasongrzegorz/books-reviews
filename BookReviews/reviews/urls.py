from django.urls import path
from . import views

app_name = 'reviews'

urlpatterns = [
    path('<int:book_id>/add/', views.AddReview.as_view(), name='add_review'),
    path('<int:book_id>/<int:review_id>/edit/', views.EditReview.as_view(), name='edit_review'),
    path('<int:book_id>/<int:review_id>/delete/', views.DeleteReview.as_view(), name='delete_review'),
    path('<int:book_id>/<int:review_id>/publish/', views.PublishReview.as_view(), name='publish_review'),
    path('list/', views.ListReviews.as_view(), name='reviews_list'),
]
