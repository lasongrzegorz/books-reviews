# Books Review App
A simple Books Review app for displaying reviews of books read by a user.
The app available at [http://my-books-reviews.herokuapp.com/](#http://my-books-reviews.herokuapp.com/)


## Table of contents
* [General info](#general-info)
* [Setup](#setup)


## General info
Books Review project idea is taken from goodreads.com or lubimyczytac.pl
 with some custom features.
A logged in user can add a book and author details. App allow user
 to rate the book and add review.
Non logged in user is allowed to view all content. Here only published
 reviews are displayed.
 

## Setup
App is being built using Django and Postgres (for training purpose).
 